//Include our classes
#include "Entity.h"
#include "singletons.h"


Entity::Entity(){
	mpAlive = true;
	setRectangle(0, 0, TILE_SIZE, TILE_SIZE);
	
	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = 0;
	mpGraphicRect.h = 0;

	mpGraphicImg = -1;
	
	mFrame = 0;
	mMaxFrame = 0;
	mCurrentFrameTime = 0;
	mMaxFrameTime = 150;

	mpDirection = NONE;
	mpSpeed = 0;
	mpMoving = false;

	mpXtoGo = mpRect.x;
	mpYtoGo = mpRect.y;
	
	mpTileBasedMovement = false;
	
}

Entity::~Entity(){
}

void Entity::init(){
	mpAlive = true;
}

void Entity::init(int x, int y) {
	init();
	mpInitialX = x;
	mpInitialY = y;

	mpRect.x = mpInitialX;
	mpRect.y = mpInitialY;

	mpXtoGo = mpRect.x;
	mpYtoGo = mpRect.y;
}

void Entity::init(int graphic, int x, int y, int w, int h) {
	init(x, y);
	mpRect.w = w;
	mpRect.h = h;
	mpGraphicImg = graphic;
	mpGraphicRect = C_Rectangle{0,0,(unsigned int)w,(unsigned int)h};
}

void Entity::update(){
	if (!mpAlive) { return; }
	updateControls();
	move();
	updateGraphic();
}

void Entity::updateGraphic() {
	mCurrentFrameTime += global_delta_time;
	if (mCurrentFrameTime > mMaxFrameTime) {
		mCurrentFrameTime = 0;
		mFrame++;
		if (mFrame >= mMaxFrame) {
			mFrame = 0;
		}
	}
	mpGraphicRect.x = mFrame*mpGraphicRect.w;
}

void Entity::render(int offX, int offY){
	if (!mpAlive || mpGraphicImg < 0) { return; }
	imgRender(mpGraphicImg, mpRect.x - offX, mpRect.y - offY, mpGraphicRect);
}

void Entity::updateControls() {
	
	return;
}

void Entity::collision(int &xx, int &yy) {
	int x = xx;
	int y = yy;
	int w = mpGraphicRect.w;
	int h = mpGraphicRect.h;
	int xfinal = xx;
	int yfinal = yy;
	switch (mpDirection) {
	case UP:
	{
		bool upLeft = sMapManager->getCollision((x + 1) / TILE_SIZE, y / TILE_SIZE);
		bool upMiddle = sMapManager->getCollision((x + w / 2) / TILE_SIZE, y / TILE_SIZE);
		bool upRight = sMapManager->getCollision((x + w - 1) / TILE_SIZE, y / TILE_SIZE);
		if (upLeft || upMiddle || upRight) {
			std::cout << "colup" << std::endl;
			int yTile = ((y) / TILE_SIZE) * TILE_SIZE + TILE_SIZE;
			int diff = ABS(yTile - (y));
			yfinal = y + diff;
		}
	}
	break;
	case DOWN:
	{
		bool downLeft = sMapManager->getCollision((x + 1) / TILE_SIZE, (y + h) / TILE_SIZE);
		bool downMiddle = sMapManager->getCollision((x + w / 2) / TILE_SIZE, (y + h) / TILE_SIZE);
		bool downRight = sMapManager->getCollision((x + w - 1) / TILE_SIZE, (y + h) / TILE_SIZE);
		if (downLeft || downMiddle || downRight) {
			std::cout << "coldown" << std::endl;
			int yTile = ((y + h) / TILE_SIZE) * TILE_SIZE;
			int diff = ABS(yTile - (y + h));
			yfinal = y - diff;

		}
	}
	break;
	case LEFT:
	{
		bool leftLeft = sMapManager->getCollision(x / TILE_SIZE, (y + 1) / TILE_SIZE);
		bool leftMiddle = sMapManager->getCollision(x / TILE_SIZE, (y + h / 2) / TILE_SIZE);
		bool leftRight = sMapManager->getCollision(x / TILE_SIZE, (y + h - 1) / TILE_SIZE);
		if (leftLeft || leftMiddle || leftRight) {
			std::cout << "colleft" << std::endl;
			int xTile = ((x) / TILE_SIZE) * TILE_SIZE + TILE_SIZE;
			int diff = ABS(xTile - (x));
			xfinal = x + diff;
		}
	}
	break;
	case RIGHT:
	{
		bool righttLeft = sMapManager->getCollision((x + w) / TILE_SIZE, (y + 1) / TILE_SIZE);
		bool tightMiddle = sMapManager->getCollision((x + w) / TILE_SIZE, (y + h / 2) / TILE_SIZE);
		bool rightRight = sMapManager->getCollision((x + w) / TILE_SIZE, (y + h - 1) / TILE_SIZE);
		if (righttLeft || tightMiddle || rightRight) {
			std::cout << "colright" << std::endl;
			int xTile = ((x + w) / TILE_SIZE) * TILE_SIZE;
			int diff = abs(xTile - (x + w));
			xfinal = x - diff;
		}
	}
	break;
	default:
		//detectar la gravedad
		/*
		bool downLeft = sMapManager->getCollision((x + 1) / TILE_SIZE, (y + h) / TILE_SIZE);
		bool downMiddle = sMapManager->getCollision((x + w / 2) / TILE_SIZE, (y + h) / TILE_SIZE);
		bool downRight = sMapManager->getCollision((x + w - 1) / TILE_SIZE, (y + h) / TILE_SIZE);
		bool maxHeight = sMapManager->getMapHeight()*(TILE_SIZE - TILE_SIZE);
		if (downLeft || downMiddle || downRight || maxHeight) {
			std::cout << "coldown" << std::endl;
			int yTile = ((y + h) / TILE_SIZE) * TILE_SIZE;
			int diff = ABS(yTile - (y + h));
			yfinal = y - diff;
			}*/
			break;
		}

	int maxW = (sMapManager->getMapWidth() - 1)* (TILE_SIZE);
	int maxH = (sMapManager->getMapHeight() - 1)* (TILE_SIZE);
	if (xfinal > maxW - w) {
		xfinal = maxW - w;
	}
	if (xfinal < 0) {
		xfinal = 0;
	}

	if (yfinal > maxH - w) {
		yfinal = maxH - w;
	}
	if (yfinal < 0) {
		yfinal = 0;
	}

	xx = xfinal;
	yy = yfinal;
	return;
}

void Entity::move() {
	int xx = mpRect.x;
	int yy = mpRect.y;
	switch (mpDirection) {
		case UP:
			yy = yy - mpSpeed*global_delta_time / 1000;
			break;
		case DOWN:
			yy = yy + mpSpeed*global_delta_time / 1000;
			break;
		case LEFT:
			xx = xx - mpSpeed*global_delta_time / 1000;
			break;
		case RIGHT:
			xx = xx + mpSpeed*global_delta_time / 1000;
			break;
		default:
			//"gravedad"
			//yy = yy + mpSpeed*global_delta_time / 1000;
			break;
	}
	collision(xx, yy);
	mpRect.x = xx;
	mpRect.y = yy;
	return;
}

bool Entity::checkCollisionWithMap() {
	return sMapManager->getCollision(mpRect.x/TILE_SIZE, mpRect.y/TILE_SIZE);
}

void Entity::setX(int x) {
	mpRect.x = x;
	return;
}

void Entity::setY(int y) {
	mpRect.y = y;
	return;
}

void Entity::setXY(int x, int y) {
	setX(x);
	setY(y);
	return;
}

void Entity::setW(int w) {
	mpRect.w = w;
	return;
}

void Entity::setH(int h) {
	mpRect.h = h;
	return;
}

void Entity::setRectangle(C_Rectangle rect) {
	mpRect = rect;
	return;
}

void Entity::setRectangle(int x, int y, int w, int h) {
	C_Rectangle a_rect = { x, y, w, h };
	setRectangle(a_rect);
	return;
}

bool Entity::isInsideRectangle(C_Rectangle a_rect) {
	if (C_RectangleTouch(mpRect, a_rect)) {
		return true;
	}
	return false;
}

void Entity::setAlive(bool alive) {
	mpAlive = alive;
	return;
}


bool Entity::isOfClass(std::string classType){
	if(classType == "Entity"){
		return true;
	}
	return false;
}

